# Software e Configuração:
- Neste projeto estou fazendo automação de testes web usando [Ruby](https://www.ruby-lang.org/pt/) como linguagem de programação, [RSpec](https://rspec.info/) ações e validações, [Cucumber](https://cucumber.io/) para interpretação de Scenarios, Steps e construção da estrutura, Estou usando o [CI/CD](https://docs.gitlab.com/ee/ci/) do proprio [GitLab](https://about.gitlab.com/) para execução diaria dos meus testes.
    - Não estou especificando versões para minhas Gems
    - Minhas declaraçoes de dependencias estão no arquivo Gemfile
    - Tudo o que será executado nesse projeto está sendo declarado no cucumber.yml

# Configuração bot Slack:
- WebHooks [Link WebHooks](https://vilariano.slack.com/apps/A0F7XDUAZ-incoming-webhooks)
    - Os Webhooks de entrada são uma maneira simples de postar mensagens de fontes externas no Slack. Eles usam solicitações HTTP normais com uma carga útil JSON, que inclui a mensagem e alguns outros detalhes opcionais descritos posteriormente.
    - [Link api](https://hooks.slack.com/services/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)
    
# Os testes estão rodando nos ambientes:
    - qa
    - prod
    - ti

## Options
* `AMB` - Em qual ambiente o teste deve ser executado
    * `qa` <- default 
    * `prod`
    * `ti`


## Comando para executar testes exemplos:
    - bundle exec cucumber AMB=qa

# Sintaxe Cron para agendamento de pipeline CI/CD GitLab
    ┌───────────── Minutos (0 - 59)
    │ ┌───────────── Horas (0 - 23)
    │ │ ┌───────────── Dias do mês (1 - 31)
    │ │ │ ┌───────────── Mês (1 - 12)
    │ │ │ │ ┌───────────── Dias da semana (0 - 6) (Sunday to Saturday;
    │ │ │ │ │                                   7 is also Sunday on some systems)
    │ │ │ │ │
    │ │ │ │ │
    0 8 * * * Sintaxe para configurar agendamentos dos testes
