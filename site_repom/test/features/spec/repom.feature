#language: pt

@AllTest
Funcionalidade: Realizar transações no site da Repom
    Sendo um cliente Repom
    Posso preencher os dados
    Para realizar transações do frete

    Contexto: Estar logado no site da Repom
        Dado que eu esteja logado no site da Repom

    @RealizarViagemSucesso
    Cenario:Emitir uma viagem com sucesso
        Quando eu preencho os dados para emissão da viagem
        Então valido que a viagem foi emitida com sucesso
    #@smoking
    @IncluirMovimentosSucesso
    Cenario:Incluir movimentos com sucesso
        Quando eu seleciono e incluo movimentos de crédito e débito
        Então valido que os movimentos foram incluídos com sucesso
    #@smoking
    @IncluirDocumentosSucesso
    Cenario:Incluir documentos com sucesso
        Quando eu seleciono e incluo documentos
        Então valido que os documentos foram incluídos com sucesso


    @CadastroPrestadorSucesso
    Cenario:Cadastrar um prestador com sucesso
        Quando eu preencho os dados para cadastro do prestador
        Então valido que o prestador foi cadastrado com sucesso

    @CadastroMotoristaSucesso
    Cenario:Cadastrar um motorista com sucesso
        Quando eu preencho os dados para cadastro do motorista
        Então valido que o motorista foi cadastrado com sucesso

    @CadastroVeiculoSucesso
    Cenario:Cadastrar um veiculo com sucesso
        Quando eu preencho os dados para cadastro do veiculo
        Então valido que o veiculo foi cadastrado com sucesso

    @PrestacaoContasSucesso
    Cenario:Prestar contas de uma viagem com sucesso
        Quando eu seleciono a viagem
        Então valido que a prestação de contas foi realizada com sucesso

    @AutorizacaoSucesso
    Cenario:Autorizar uma viagem com sucesso
        Quando eu preenchdo os dados para autorização
        Então valido que a autorização foi realizada com sucesso

    @RoteiroSucesso
    Cenario:Criar um roteiro com sucesso
        Quando eu preenchdo os dados para criação do reoteiro
        Então valido que o roteiro foi criado com sucesso

    @ConsultaroteiroSucesso
    Cenario:Consultar um roteiro com sucesso
        Quando eu preenchdo os dados para consulta do reoteiro
        Então valido que o roteiro foi consultado com sucesso

    @CancelarautorizacaoSucesso
    Cenario:Cancelar a autorização de  uma viagem com sucesso
        Quando eu preenchdo os dados para o cancelamento da autorização
        Então valido que o cancelamento da autorização foi realizada com sucesso

    @CancelprestacaoSucesso
    Cenario:Cancelar a prestação de contras de uma viagem com sucesso
        Quando eu preenchdo os dados para o cancelamento da prestação de contas
        Então valido que o cancelamento da prestação de contas foi realizada com sucesso

    @CancelviagemSucesso
    Cenario:Cancelar  uma viagem com sucesso
        Quando eu preenchdo os dados para o cancelamento da viage,
        Então valido que o cancelamento da viagem foi realizada com sucesso

    @AtendimentoTransferencia
    Cenario: Realizar uma transferencia via atendimento
        Dado que eu esteja logado no atendimento
        Quando eu realizar uma transferencia
        Entao a transferencia sera realizada com sucesso

    @CancelaTransferencia
    Cenario: Realizar uma transferencia via atendimento
        Dado que eu esteja logado no atendimento
        Quando eu realizar cancelamento de transferencia
        Entao a transferencia sera estornada com sucesso

    @VendaAtendimento
    Cenario: Realizar venda via atendimento
        Dado que eu esteja logado no atendimento
        Quando eu realizar uma venda via atendimento
        Entao a venda sera realizada com sucesso

    @CancelarVenda
    Cenario: Realizar o cancelamento de venda via atendimento
        Dado que eu esteja logado no atendimento
        Quando eu realizar o cancelamento da venda via atendimento
        Entao a venda sera cancelada com sucesso
