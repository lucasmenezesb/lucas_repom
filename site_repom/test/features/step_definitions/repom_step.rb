Dado("que eu esteja logado no site da Repom") do
  repom.load
  repom.faz_login
end

Quando("eu preencho os dados para cadastro do prestador") do
  prestador.cadastro_prestador
end

Então("valido que o prestador foi cadastrado com sucesso") do
  @valida_prestador = expect(page).to have_text($RETURNS["nome_prestador"])
  raise = "Cadastro não realizado" if @valida_prestador == false
  $RETURNS["documento_fiscal"] << $cpf
  $RETURNS["nome_prestador"] << $name_faker
  p $RETURNS
  #repom.liberando_viagem
end

Quando("eu preencho os dados para cadastro do motorista") do
  motorista.cadastro_motorista
end

Então("valido que o motorista foi cadastrado com sucesso") do
  @valida_motorista = expect(page).to have_text($RETURNS["nome_motorista"])
  raise = "Cadastro não realizado" if @valida_motorista == false
  expect(page).to have_text($RETURNS["cpf"])
  $RETURNS["cpf"] << $cpf
  $RETURNS["nome_motorista"] << $name_faker
  #repom.liberando_viagem
  p $RETURNS
end

Quando("eu preencho os dados para cadastro do veiculo") do
  veiculo.cadastrar_veiculo
end

Então("valido que o veiculo foi cadastrado com sucesso") do
  # pending # Write code here that turns the phrase above into concrete actions
  $RETURNS["veiculo"] << $placa
  log "falta implementar step de validacao"
  p $RETURNS
end

Quando("eu preencho os dados para emissão da viagem") do
  repom.emitir_viagem
  #repom.liberando_viagem
end

Então("valido que a viagem foi emitida com sucesso") do
  @valida_viagem = expect(page).to have_text "Viagem emitida com sucesso!"
  raise "[ERRO} a viagem não foi emitida com sucesso" if @valida_viagem == false
  expect(page).to have_selector "h2.swal2-title", text: "Viagem emitida com sucesso!"
  p $RETURNS
  #repom.liberando_viagem
end

Quando("eu seleciono a viagem") do
  #repom.liberando_viagem
  prestar_contas.prestar_contas
  log $RETURNS["cod_viagem"]
end

Então("valido que a prestação de contas foi realizada com sucesso") do
  @validacao_prestacao = expect(page).to have_text "Prestação de Contas foi realizada com sucesso!"
  raise "[ERRO] a viagem não foi prestada contas com sucesso" if @validacao_prestacao == false
  #repom.liberando_viagem
end

Quando("eu preenchdo os dados para autorização") do
  autorizacao.autorizar_viagem
end

Então("valido que a autorização foi realizada com sucesso") do
  log "falta implementar step de validacao"
end

Quando("eu preenchdo os dados para criação do reoteiro") do
  roteiro.emitir_roteiro
end

Então("valido que o roteiro foi criado com sucesso") do
  @valida_roteiro = expect(page).to have_text "Solicitação de Roteiro cadastrado com sucesso"
  raise "[ERRO} o roteiro não foi emitido com sucesso" if @valida_roteiro == false
  expect(page).to have_selector "#txtMensagemSucesso", text: "Solicitação de Roteiro cadastrado com sucesso"
end

Quando("eu preenchdo os dados para o cancelamento da autorização") do
  cancelamento_autorizacao.cancelarautorizacao_viagem
end

Então("valido que o cancelamento da autorização foi realizada com sucesso") do
  @validacancelamento_autorizacao = expect(page).to have_text "Cancelamento de autorização de pagamento realizada com sucesso"
  raise "[ERRO} o cancelamento da autorização naõ foi realizada" if @validacancelamento_autorizacao == false
end

Quando("eu preenchdo os dados para o cancelamento da prestação de contas") do
  cancelamento_prestacao.cancelarprestar_contas
end

Então("valido que o cancelamento da prestação de contas foi realizada com sucesso") do
  @validar_cancelprest = expect(page).to have_text "Prestação de Contas foi estornada com sucesso!"
  raise "[ERRO] o cancelamento não foi efetivado " if @validar_cancelprest == false
end

Quando("eu preenchdo os dados para o cancelamento da viage,") do
  cancelamento_viagem.cancelar_viagem
end

Então("valido que o cancelamento da viagem foi realizada com sucesso") do
  @cancelamento_viagem = expect(page).to have_text "A Viagem foi Cancelada com sucesso"
  raise "[ERRO] a viagem não foi cancelada" if @cancelamento_viagem == false
end

Quando("eu preenchdo os dados para consulta do reoteiro") do
  consulta_roteiro.consultar_roteiro
  log $RETURNS["cod_cliente"]
end

Então("valido que o roteiro foi consultado com sucesso") do
  @consulta_rot = expect(page).to have_text ($RETURNS["cod_cliente"])
end

Quando("eu seleciono e incluo movimentos de crédito e débito") do
  incluir_movimentos.incluir_movimentos
end

Então("valido que os movimentos foram incluídos com sucesso") do
  @inclusao_movimentosdebito = expect(page).to have_text "teste_debito"
  @inclusao_movimentocredito = expect(page).to have_text "teste_credito"
end

Quando("eu seleciono e incluo documentos") do
  incluir_documentos.incluir_documentos
end

Então("valido que os documentos foram incluídos com sucesso") do
  @incluir_documebtos = expect(page).to have_text "Documentos da Viagem incluídos com sucesso"
end

Dado('que eu esteja logado no atendimento') do
  @atendimento = AtendimentoPage.new
  @atendimento.load
  sleep 2
  @atendimento.login_atendimento
end

Quando('eu realizar uma transferencia') do
  @atendimento.atendimento_menu
  @atendimento.realizar_transferencia
  sleep 4
end

Entao('a transferencia sera realizada com sucesso') do
  @transfer = expect(page).to have_text 'Transferência efetuada com sucesso'
end

Quando('eu realizar cancelamento de transferencia') do
  @atendimento.atendimento_menu
  cancelar_transferencia.cancelamento_transferencia
end

Entao('a transferencia sera estornada com sucesso') do
  @cancela_transferencia = expect(page).to have_text 'Agendamento de Transferência Cancelado com sucesso.'
end

Quando('eu realizar uma venda via atendimento') do
  @atendimento.atendimento_menu
  venda_atendimento.vendaatendimento
end

Entao('a venda sera realizada com sucesso') do
  @venda_atendimento = expect(page).to have_text 'Comprovante de Venda'
end

Quando('eu realizar o cancelamento da venda via atendimento') do
  @atendimento.atendimento_menu
  cancelar_venda.cancelarvenda
end

Entao('a venda sera cancelada com sucesso') do
  @cancelar_venda = expect(page).to have_text 'Estorno Realizado com Sucesso !'
end