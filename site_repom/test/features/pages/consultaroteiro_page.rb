class ConsultaroteiroPage < SitePrism::Page
  def consultar_roteiro
    click_on "Roteiro"
    click_on "Consultar"
    find("input#DataSolicitacaoInicial").send_keys $data_atual
    find("input#DataSolicitacaoFinal").send_keys $data_atual
    click_on "Procurar"
    sleep 10
  end
end
