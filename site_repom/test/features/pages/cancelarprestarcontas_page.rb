class CancelarprestacaocontasPage < SitePrism::Page
  def cancelarprestar_contas
    find(ELL["link_contrato"]).click
    find(ELL["consultar_viagem"]).click
    # find("#StatusID option[value='3']").select_option
    # find(ELL["btn_procurar"]).click
    # cod_viagem = find("table.footable>tbody>tr:nth-child(1)>td:nth-child(2)").text
    # $RETURNS["cod_viagem"] << cod_viagem
    find("input#ContratoViagemIDView").send_keys($RETURNS["cod_viagem"])
    find(ELL["btn_procurar"]).click
    case page.has_text?("Quitado")
    when true
      find(ELL["icone"], :visible => true).click
      case page.has_text?("Prestação de Contas")
      when true
        click_on "Prestação de Contas"
        click_on "Estornar Prestação de Contas"
      end
    end
  end
end
