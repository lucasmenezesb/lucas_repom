class RepomPage < SitePrism::Page
  set_url ""

  def faz_login
    find(ELL["user_name"]).set(LOADS["usuario"])
    find(ELL["password"]).set(LOADS["senha"])
    find(ELL["btn_entrar"]).click
    # find(ELL['acessar_cliente']).click
    find(ELL["fechar_modal"]).click
  end

  def emitir_viagem
    find(ELL["link_contrato"]).click
    find(ELL["link_emitir"]).click
    find(ELL["filial"]).click
    find(ELL["escolha_filial"]).click
    find(ELL["operacao"]).click
    sleep 2
    find(ELL["escolha_operacao"], visible: true).click
    sleep 2
    find(ELL["btn_continuar"], :visible => true).click
    sleep 2
    find(ELL["origem"], :visible => true).click
    sleep 2.5
    find(ELL["setando_origem"]).send_keys(LOADS["origem"])
    find(ELL["clicando_origem"], :wait => 10).click
    sleep 2.5
    find(ELL["destino"], :wait => 30).click
    find(ELL["setando_destino"]).send_keys(LOADS["destino"])
    sleep 2.5
    find(ELL["clicando_destino"], :wait => 10).click
    sleep 2.5
    find(ELL["escolha_rota"]).click
    sleep 2.5
    find("button", text: "Escolher rota").click
    find(ELL["cep_origem_ciot"]).click
    find(ELL["cep_origem_ciot"]).send_keys(LOADS["cep_origem_ciot"])
    find(ELL["cep_destino_ciot"]).click
    find(ELL["cep_destino_ciot"]).send_keys(LOADS["cep_destino_ciot"])
    find(ELL["distancia_ciot"]).click
    find(ELL["distancia_ciot"]).send_keys(LOADS["distancia_ciot"])

    # find(ELL["escolha_rota"]).click
    # sleep 2.5
    # find("button", text: "Escolher rota").click
    # sleep 2.5
    find(ELL["btn_continuar2"]).click
    find(ELL["cpf_cnpj_ciot"]).click
    find(ELL["cpf_cnpj_ciot"]).send_keys $cnpj
    find(ELL["name_ciot"]).click
    find(ELL["name_ciot"]).send_keys $name_faker
    find(ELL["btn_ciot"], visible: true).click
    sleep 1.5
    find(ELL["prestador"], visible: true).click
    sleep 2.5
    find(ELL["setando_prestador"]).send_keys(LOADS["prestador"])
    find(ELL["clicando_prestador"], visible: true).click
    sleep 2.5
    find(ELL["btn_continuar3"], visible: true).click
    sleep 2.5
    find(ELL["motorista"], visible: true).click
    find(ELL["setando_motorista"], visible: true).send_keys(LOADS["motorista"])
    find(ELL["clicando_motorista"], visible: true).click
    sleep 1
    find(ELL["btn_continuar4"], visible: true).double_click
    # sleep 1.5
    find(ELL["veiculos"], visible: true).click
    find(ELL["setando_veiculos"], visible: true).send_keys(LOADS["placa"])
    find(ELL["clicando_veiculo"], visible: true).click
    #   case page.has_css?("btn_continuar5")
    #   when true
    sleep 1.5
    find(ELL["btn_continuar5"], visible: true).double_click
    #find(ELL["btn_continuar5"], visible: true).double_click
    sleep 1.5
    find(ELL["documentos"], visible: true).click
    sleep 2.5
    find(ELL["escolha_documentos"]).click
    sleep 2.5
    find(ELL["filial_docs"], visible: true).click
    sleep 1.5
    find(ELL["escolha_filialdocs"], visible: true).click
    sleep 1.5
    find(ELL["codigo_doc"], visible: true).send_keys $number1
    sleep 1.5
    find(ELL["filial_doc"], visible: true).set $number2
    sleep 1.5
    find(ELL["btn_continuar6"], visible: true).double_click
    sleep 1.5
    find(ELL["data_saida"], visible: true).click
    sleep 1.5
    find(ELL["data_saida"], visible: true).send_keys $data
    sleep 1.5
    find(ELL["data_saida"], visible: true).send_keys :tab
    sleep 1.5
    find(ELL["hora_saida"], visible: true).send_keys $hora_atual
    sleep 1.5
    find(ELL["hora_saida"], visible: true).send_keys :tab
    sleep 1.5
    find(ELL["data_prevista"], visible: true).send_keys $data_futura
    sleep 1.5
    find(ELL["data_prevista"], visible: true).send_keys :tab
    sleep 1.5
    find(ELL["hora_prevista"], visible: true).send_keys $hora_atual
    sleep 1.5
    find(ELL["hora_prevista"], visible: true).send_keys :tab
    sleep 1.5
    find(ELL["btn_continuar7"]).double_click
    sleep 1.5
    find(ELL["mercadoria"], visible: true).click
    sleep 1.5
    find(ELL["setar_mercadoria"], visible: false).send_keys(LOADS["mercadoria"])
    sleep 1.5
    find(ELL["clicando_mercadoria"], visible: true).click
    sleep 1.5
    find(ELL["codigo_carga"], visible: true).click
    find(ELL["select_carga"], visible: true).click
    find(ELL["valor_mercadoria"]).click
    find(ELL["valor_mercadoria"]).send_keys $number1
    find(ELL["peso_mercadoria"]).click
    find(ELL["peso_mercadoria"]).send_keys $peso_carga
    sleep 1.5
    find(ELL["btn_continuar8"], visible: true).double_click
    sleep 1.5
    find(ELL["valor_frete"]).send_keys $number1
    sleep 1.5
    find(ELL["btn_vlr_frete"], visible: true).double_click
    sleep 1.5
    find(ELL["btn_movimentos"], visible: true).double_click
    sleep 1.8
    # choose("emissionVPRNo")
    # sleep 1.5
    # find(ELL["btn_continuar11"], :visible => true).double_click
    # sleep 1.5
    find(ELL["btn_continuar12"], visible: true).double_click
    sleep 1
    find(ELL["btn_finish"]).click
    sleep 10
  end

  def faz_logout
    find(ELL["btn_logout"]).click
    click_link "Sair"
  end

  def liberando_viagem
    if case page.has_text?("OK")
    when true
      click_on "OK"
      find(ELL["btn_finish"]).click
    else case page.has_text?("Viagem emitida com sucesso!")
    when true
      4.times do
        response = HTTParty.post("https://qa.repom.com.br/Repom.Frete.Web/TesteQualidade/LiberarEmissaoPendente")
        puts "response code : #{response.code}"
      end
    end
    end
    end
  end
end
