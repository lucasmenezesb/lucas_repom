class IncluirdocumentosPage < SitePrism::Page
  def incluir_documentos
    find(ELL["link_contrato"]).click
    find(ELL["consultar_viagem"]).click
    find(ELL["btn_procurar"]).click
    incluir_docs = find("table.footable>tbody>tr:nth-child(1)>td:nth-child(2)").text
    $RETURNS["incluir_docs"] << incluir_docs
    puts $RETURNS["incluir_docs"]
    find("input#ContratoViagemIDView").send_keys $RETURNS["incluir_docs"]
    find(ELL["btn_procurar"]).click
    case page.has_text?("Em Transito")
    when true
      find(ELL["icone"], :visible => true).click
      case page.has_text?("Incluir Documentos")
      when true
        click_on "Incluir Documentos"
        find("#select-novo-documento-tipo-id [value ='3']").select_option
        find("span.ladda-label").click
        sleep 1
        click_on "Ok"
        find("input#txt-novo-documento-serie").send_keys $number2
        sleep 1
        find("input#txt-novo-documento-codigo").send_keys $number3
        sleep 1
        find("button#btn-novo-documento").click
        sleep 1
        click_on "Salvar"
        sleep 10
      end
    end
  end
end
