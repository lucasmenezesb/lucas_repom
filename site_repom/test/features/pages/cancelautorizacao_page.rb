class CancelautorizacaoPage < SitePrism::Page
  def cancelarautorizacao_viagem
    find(ELL["link_contrato"]).click
    click_on "Cancelar Autorização"
    find(ELL["operacao_autorizacao"]).click
    find(ELL["operacao_autorizacao"]).send_keys "1 - Op. Prazo Pgto 1-Corrido", :enter
    find(ELL["filial_autorizacao"]).click
    find(ELL["filial_autorizacao"]).send_keys "54.237.334/0001-02 - FREIGHT AUT/QA", :enter
    find(ELL["data_inicial_aut"]).click
    find(ELL["data_inicial_aut"]).send_keys $data_futura
    find(ELL["data_final_aut"]).click
    find(ELL["data_final_aut"]).send_keys $data_futura
    find(ELL["btn_procurar_aut"]).click
    find("ul#treeview-list div.col-xs-4.nivel1 > div > label > input").click
    find("button#btAutorizar").click
    click_on "Confirmar"
    sleep 5
  end
end
