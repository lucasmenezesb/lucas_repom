class IncluirmovimentosPage < SitePrism::Page
  def incluir_movimentos
    find(ELL["link_contrato"]).click
    find(ELL["consultar_viagem"]).click
    find(ELL["btn_procurar"]).click
    cod_viagem = find("table.footable>tbody>tr:nth-child(1)>td:nth-child(2)").text
    $RETURNS["cod_viagem"] << cod_viagem
    puts $RETURNS["cod_viagem"]
    find("input#ContratoViagemIDView").send_keys $RETURNS["cod_viagem"]
    find(ELL["btn_procurar"]).click
    case page.has_text?("Em Transito")
    when true
      find(ELL["icone"], :visible => true).click
      case page.has_text?("Incluir Débito e Crédito")
      when true
        click_on "Incluir Débito e Crédito"
        find("#MovimentoTipoID [value='102']").select_option
        find("input#Valor").send_keys $number3
        find("button#btn-incluir").click
        click_on "Sim"
        click_on "OK"
        find("#MovimentoTipoID [value='101']").select_option
        find("input#Valor").send_keys $number3
        find("button#btn-incluir").click
        click_on "Sim"
        click_on "OK"
        sleep 10
      end
    end
  end
end
