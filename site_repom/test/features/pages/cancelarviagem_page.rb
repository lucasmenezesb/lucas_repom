class CancelarviagemPage < SitePrism::Page
  def cancelar_viagem
    find(ELL["link_contrato"]).click
    find(ELL["consultar_viagem"]).click
    find("input#ContratoViagemIDView").send_keys($RETURNS["cod_viagem"])
    find(ELL["btn_procurar"]).click
    find("table.footable>tbody>tr:nth-child(1) .btn-group.dropup").click
    case page.has_text?("Cancelar/Interromper")
    when true
      click_on "Cancelar/Interromper"
      click_on "Cancelar"
      click_on "Sim"
      sleep 10
    end
  end
end
