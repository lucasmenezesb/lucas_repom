class PrestadorcontasPage < SitePrism::Page
  def prestar_contas
    find(ELL["link_contrato"]).click
    find(ELL["consultar_viagem"]).click
    find(ELL["btn_procurar"]).click
    prest_contas = find("table.footable>tbody>tr:nth-child(1)>td:nth-child(2)").text
    $RETURNS["prest_contas"] << prest_contas
    find("input#ContratoViagemIDView").send_keys $RETURNS["prest_contas"]
    find(ELL["btn_procurar"]).click
    case page.has_text?("Em Transito")
    when true
      find(ELL["icone"], :visible => true).click
      case page.has_text?("Prestação de Contas")
      when true
        find(ELL["link_prestar"]).click
        find(ELL["btn_prestar_efetivamente"], :visible => true).click
        find(ELL["btn_consulta"]).click
        find(ELL["check_box"]).click
        find(ELL["peso_carga"]).click
        find(ELL["peso_carga"]).send_keys $peso_carga
        find(ELL["btn_salvar"]).click
        #   if case page.has_text?("Atenção")
        #   when true
        #     click_on "OK"
        #     find(ELL["btn_salvar"]).click
        #   else case page.has_text?("Prestação de Contas foi realizada com sucesso!")
        #   when true
        #     # click_on "OK"
        #   end
        #   end
        #   end
      end
    end
  end
end
