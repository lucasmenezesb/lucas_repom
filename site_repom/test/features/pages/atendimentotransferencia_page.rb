class AtendimentoPage < SitePrism::Page
  set_url 'https://apph04.repom.com.br/Repom.Atendimento.Web/Account/Login'

  def login_atendimento
    find(ELL["user_name_atendimento"]).set(LOADS["usuario_atendimento"])
    find(ELL["password_atendimento"]).set(LOADS["senha_atendimento"])
    find(ELL["btn_entrar_atendimento"]).click       
  end

  def atendimento_menu
    find(ELL["menu_atendimento"]).click
    find(ELL["cpfcnpjprestador"]).set(LOADS["prestador_atendimento"])
    find(ELL["btn_pesquisar"]).click
    sleep 3
    find(ELL["check_one"]).click
    find(ELL["check_two"]).click
    find(ELL["check_tree"]).click
    find(ELL["btn_cont"]).click
    sleep 10
  end

  def realizar_transferencia
    find(ELL["btn_transfer"]).click
    find(ELL["insert_value"]).set(LOADS["valor_transfer"])
    find(ELL["btn_calc"]).click
    find(ELL["btn_transfer_confirmar"]).click
    find(ELL["btn_conf"]).click
  end
end