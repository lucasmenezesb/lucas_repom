class VendaatendimentoPage < SitePrism::Page
    def vendaatendimento
       find(ELL["select_filtro"]).click
       find(ELL["btn_pesq"]).click
       find(ELL["cart_ven"]).click
       find(ELL["btn_incluivenda"]).click
       sleep 2
       find(ELL["preencher_estab"]).set(LOADS["valor_estabelecimento"])
       find(ELL["btn_consul"]).click
       find(ELL["valor_diesel"]).set(LOADS["valor_combustivel"])
       find(ELL["adiciona_combustivel"]).click
       find(ELL["confirma_venda"]).click
       find(ELL["confirmar_venda"]).click
       sleep 3    
    end
end