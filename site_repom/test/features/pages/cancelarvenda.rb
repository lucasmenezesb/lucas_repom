class CancelarvendaPage < SitePrism::Page
    def cancelarvenda
      find(ELL["select_filtro"]).click
      find(ELL["btn_pesq"]).click
      find(ELL["cart_ven"]).click
      find(ELL["btn_consvenda"]).click
      first(ELL["ver_venda"]).click
      find(ELL["btn_cancelvenda"]).click
      find(ELL["btn_sim"]).click
    end
end