class CancelamentotransferenciaPage < SitePrism::Page
    def cancelamento_transferencia
        find(ELL["btn_historico"]).click
        first(ELL["btn_inf_transf"]).click
        find(ELL["btn_cancel_trans"]).click
        find(ELL["btn_conf"]).click
      end   
end