class PrestadorPage < SitePrism::Page
  def cadastro_prestador
    find(ELL['link_cadastro']).click
    find(ELL['link_prestador']).click
    find(ELL['btn_prestador']).click
    find(ELL['documento_fiscal']).send_keys $cpf
    find(ELL['nome_prestador']).send_keys $name_faker
    find(ELL['rntrc']).send_keys $rntrc
    find(ELL['celular']).send_keys(LOADS['phone'])
    find(ELL['btn_cadastrar'],:visible => true).click
    sleep 10
  end


end