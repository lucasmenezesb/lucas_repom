class AutorizacaoPage < SitePrism::Page
  def autorizar_viagem
    find(ELL["link_contrato"]).click
    find(ELL["link_autorizacao"]).click
    find(ELL["operacao_autorizacao"]).click
    find(ELL["operacao_autorizacao"]).send_keys "1 - Op. Prazo Pgto 1-Corrido", :enter
    find(ELL["filial_autorizacao"]).click
    find(ELL["filial_autorizacao"]).send_keys "54.237.334/0001-02 - FREIGHT AUT/QA", :enter
    find(ELL["data_inicial_aut"]).click
    find(ELL["data_inicial_aut"]).send_keys $data_atual
    find(ELL["data_final_aut"]).click
    find(ELL["data_final_aut"]).send_keys $data_atual
    find(ELL["btn_procurar_aut"]).click
    find(".col-xs-4.nivel1 .fa.fa-arrow-circle-right").click
    find(".col-xs-4.nivel2 .fa.fa-arrow-circle-right").click
    find(".col-xs-4.nivel3 .fa.fa-arrow-circle-right").click
    find(".form-control[name='datamovimento']").native.clear
    find(".form-control[name='datamovimento']").send_keys $data_futura
    find("ul#treeview-list div.col-xs-4.nivel1 > div > label > input").click
    case page.has_css?(ELL["btn_procurar_aut"])
    when true
      find(ELL["btn_autorizar"], :visible => true).click
      find(ELL["btn_confirmar_aut"], :visible => true).click
      sleep 5
    end
  end
end
