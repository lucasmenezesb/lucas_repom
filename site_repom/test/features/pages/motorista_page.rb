class MotoristaPage < SitePrism::Page
  def cadastro_motorista
    case page.has_text?("Cadastros")
    when true
      find('span', text: "Cadastros", visible: true).click 
      case page.has_text?("Motorista")
      when true
        find('a', text: "Motorista", visible: true).double_click
        case page.has_text?("Incluir")
        when true
          find('a', text: "Incluir", visible: true).double_click
        end
      end
    end
    using_wait_time 6 do
      within 'form#form-motorista' do
        find(ELL["cpf_motorista"]).send_keys $cpf
        find(ELL["cpf_verificar"]).click
        find(ELL['cnh']).set $cnh
        find(ELL['nome_motorista']).set $name_faker
        find(ELL['ddd']).set(LOADS['ddd'])
        find(ELL['ddd']).send_keys :tab
        find(ELL['cel']).send_keys(LOADS['cel'])
        find(ELL['btn_motorista'],:visible => true).click
        sleep 5
      end
     end
     using_wait_time 6 do
      within '.swal2-modal.swal2-show' do
       click_button 'Visualizar'
        sleep 5
      end
     end
  end
end
