# Chamando todas as dependencias q
require "capybara"
require "capybara/cucumber"
require "capybara/dsl"
require "capybara/rspec/matchers"
require "faker"
require "rspec"
require "selenium-webdriver"
require "site_prism"
require "report_builder"
require "httparty"
require "json"
require "pry"
require "cpf_faker"

require_relative "page_helper.rb" ##Arquivo que inicia todas as minhas classes

World Capybara::DSL
World Capybara::RSpecMatchers

World Page

BROWSER = ENV["BROWSER"] ##tagueamento para escolha de Browsaer
AMB = ENV["AMB"] || ""

raise "Constante AMB está vazia. \n POr favor, especificar Ambiente: AMB=prod,AMB=ti,AMB=qa" if AMB == ""

BASE_URL = YAML.load_file(File.dirname(__FILE__) + "/ambientes/ambientes.yml")[AMB] ## Caminho de arquivo com definiçoes de ambiente
LOADS = YAML.load_file(File.dirname(__FILE__) + "/loads/loads.yml")
ELL = YAML.load_file(File.dirname(__FILE__) + "/loads/elements.yml")
$RETURNS = YAML.load_file(File.dirname(__FILE__) + "/loads/returns.yml")

$REPORTJSON = JSON.load(File.open(File.dirname(__FILE__) + "/../../results/report.json"))
## executa navegador de acordo com o navegador escolhido em cucumber.yml
# :selenium_headless, :selenium_chrome, :selenium_chrome_headless
case BROWSER
when "chrome"
  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << "--disable-site-isolation-trials"
      opts.args << "--start-maximized"
      opts.args << "--incognito"
    end
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
  end
  @driver = :selenium_chrome
when "chrome_headless"
  Capybara.register_driver :selenium_chrome_headless do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << "--headless"
      opts.args << "--disable-gpu" if Gem.win_platform?
      opts.args << "--no-sandbox"
      opts.args << "--incognito"
      opts.args << "--window-size=1200x680"
      opts.args << "--disable-site-isolation-trials"
    end
    Capybara::Selenium::Driver.new(
      app,
      browser: :chrome,
      options: browser_options,
    )
  end
  @driver = :selenium_chrome_headless
when "two_chrome_headless"
  Capybara.register_driver :selenium_chrome_headless_docker_friendly do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new
    browser_options.args << "--headless"
    browser_options.args << "--disable-gpu"
    browser_options.args << "--no-sandbox"
    browser_options.args << "--disable-dev-shm-usage"
    browser_options.args << "--window-size=1871,981"

    Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
  end
  @driver = Capybara.javascript_driver = :selenium_chrome_headless_docker_friendly
when BROWSER.blank?
  raise "Constante BROWSER está vazia.\nPor favor, especificar BROWSER: [chrome, chrome_headless]."
end

# ReportBuilder.configure do |config|
#   config.json_path = "results/report.json" #pasta onde salva o json
#   config.report_path = "results/report" #pasta onde salva o html
#   config.report_types = [:html] #tipo de report a exportar
#   config.report_title = "Site_REPOM" #nome do report
#   config.color = "blue" #cor do report
#   config.include_images = true #se coloca imagens ou não
#   config.additional_info = { browser: ENV["BROWSER"], amb: ENV["AMB"] }
# end
#essa variável ambiente recebe uma configuração do ENV. voce pode apagar os IFs ali e deixar o nome chumbado
# at_exit do
#   ReportBuilder.build_report
# end

Capybara.configure do |config|
  config.default_driver = @driver ## Variavel para definissão de Browser
  config.app_host = BASE_URL["base_url"]
  config.default_max_wait_time = 40 ## Time global de espera
  config.default_selector = :css
end
