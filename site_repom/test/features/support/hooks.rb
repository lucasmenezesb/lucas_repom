After do |scenario|
  scenario_name = scenario.name.gsub(/[^\w\-]/, " ")

  if scenario.failed?
    # Se meu senario falhar tira um print e salva no caminho que defino em helper.rb
    tira_foto(scenario_name.downcase!, "falhou")
  else
    # Se meu senario falhar tira um print e salva no caminho que defino em helper.rb
    tira_foto(scenario_name.downcase!, "passou")
  end
end
#After do |scenario|
# repom.faz_logout

#     #expect(page.current_url).to eql('http://automationpractice.com/index.php?controller=authentication&back=my-account')
#end
# Tira screenshot da pagina
# def tira_foto(file_name, resultado)
#   temp_shot = page.save_screenshot("results/evidencias/#{$data}/temp_screen#{$h_m_s}.png")
#   shot = Base64.encode64(File.open(temp_shot, "rb").read)
#   attach(shot, "image/png")
# end

def tira_foto(file_name, resultado)
  data = Time.now.strftime("%F").to_s
  h_m_s = Time.now.strftime("%H%M%S%p").to_s
  temp_shot = page.save_screenshot("results/evidencias/#{data}/temp_screen#{h_m_s}.png")
  shot = Base64.encode64(File.open(temp_shot).read)
  attach(temp_shot, "image/png")
end

def temp_shot
  temp_shot = page.save_screenshot("results/evidencias/#{$data}/temp_shot#{$h_m_s}.png")
  shot = Base64.encode64(File.open(temp_shot, "rb").read)
end

# Cria massa Faker para que eu possa usar em qualquer momento do meu projeto
Before do
  Faker::Config.locale = "pt-BR"
  $email_faker = Faker::Internet.email
  $name_faker = Faker::Name.name
  $first_name_faker = Faker::Name.first_name
  $last_name_faker = Faker::Name.last_name
  $number1 = Faker::Number.number(digits: 5)
  $peso_carga = "100000"
  $number2 = Faker::Number.number(digits: 2)
  $number3 = Faker::Number.number(digits: 4)
  $h_m_s = Time.now.strftime("%H%M%S%p").to_s
  $data = Time.now.strftime("%d%m%Y").to_s
  $password = "123345"
  $company = "Vidal Consultoria"
  $street_address_faker = Faker::Address.street_address
  $city_faker = Faker::Address.city
  $zip_code = "00000"
  $mobile_phone = "01199999999"
  $birthdate = "04/04/1987"
  time = Time.now
  data_saida = time + 86400 #segundos que equivale a um dia
  $data = data_saida.strftime("%d%m%Y")
  $hora_atual = Time.now.strftime("%H%M") #horas
  data_prevista = time + 259200 #segundos que equivale a 3 dias
  $data_futura = data_prevista.strftime("%d%m%Y")
  $data_atual = Time.now.strftime("%d%m%Y")
  $cpf = Faker::CPF.numeric
  $cnpj = Faker::CNPJ.numeric
  $rntrc = Faker::Number.number(digits: 9)
  $cnh = Faker::Number.number(digits: 11)
  $placa = Faker::Alphanumeric.alphanumeric(number: 7, min_alpha: 3, min_numeric: 4) #=> "3yfq2phx8b"
  $cod_cliente = Faker::Alphanumeric.alphanumeric(number: 10, min_alpha: 3, min_numeric: 3)
end

at_exit do
  # Aqui guardo algumas algumas variaveis com valores 0 pois eles serão alterados depois que a execução dos testes forem finalizados
  total_cenario = 0
  sucesso_cenario = 0
  falhas_cenario = 0
  total_step = 0
  sucesso_step = 0
  falhas_step = 0
  interrupcoes_step = 0
  # Aqui eu inporto o caminho para uma variavel
  report_path = "#{Dir.pwd}/results"
  # Aqui eu importo o json gerado no fim dos testes
  report = JSON.parse(File.read("#{report_path}/report.json"))
  # Aqui eu faco um each para mapear alguns dados do relatorio
  report.each do |feature|
    feature["elements"].each do |scenario|
      total_cenario += 1
      if scenario["steps"].last["result"]["status"] == "passed"
        sucesso_cenario += 1
      else
        falhas_cenario += 1
      end
      scenario["steps"].each do |step|
        total_step += 1
        if step["result"]["status"] == "skipped"
          interrupcoes_step += 1
        elsif step["result"]["status"] == "passed"
          sucesso_step += 1
        elsif step["result"]["status"] == "failed"
          falhas_step += 1
        end
      end
    end
  end
  # Aqui defino uma variavel de cor para testes OK e Falhas
  color = if falhas_cenario >= 1
      "#ff9999"
    else
      "#b3ffcc"
    end
  # Aqui defino uma variavel de Tempo para pegar valores de Data, Hora, Minuto e Segundos.
  date = Time.now
  date = date.strftime("%d-%m-%Y - %H:%M:%S")
  # Aqui defino uma variavel com template para slack
  $slack_payload = {
    "attachments": [
      {
        "color": "#{color}",
        "title": "Ambiente de execução: #{AMB.upcase}",
      },
      {
        "color": "#{color}",
        "title": "LINK - Pipelines",
        "title_link": "https://gitlab.com/vidalaffonso/site_repom/pipelines",
      },
      {
        "color": "#{color}",
        "title": "Branch Master deveup-api",
        "title_link": "https://gitlab.com/vidalaffonso/site_repom",
      },
      {
        "color": "#{color}",
        "title": "Results tests",
        "text": "Data e hortas da execução: #{date}:",
        "fields": [
          {
            "title": "Cenários",
            "value": "Total\nSucessos\nFalhas\n",
            "short": true,
          },
          {
            "title": "Quantidade",
            "value": "#{total_cenario}\n#{sucesso_cenario}\n#{falhas_cenario}",
            "short": true,
          },
          {
            "title": "Steps",
            "value": "Total\nSucessos\nFalhas\nNão Concluidos",
            "short": true,
          },
          {
            "title": "Quantidade",
            "value": "#{total_step}\n#{sucesso_step}\n#{falhas_step}\n#{interrupcoes_step}",
            "short": true,
          },
        ],
      },
    ],
  }
  # Aqui faço um post na api do slack incorpando o template definido na variavel $slack_payload
  @return = HTTParty.post(BASE_URL["slack"], body: $slack_payload.to_json)
  # Aqui verifico se ocorreu tudo ok com meu envio de informaçãoes
  if @return.code != 200
    raise "[ERROR] Problema na api de notificação do Slack \n\n"
    puts @return.code
    puts @return.body
  else
  end
end
