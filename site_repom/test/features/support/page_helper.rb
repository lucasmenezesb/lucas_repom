Dir[File.join(File.dirname(__FILE__), "../pages/*_page.rb")].each { |file| require file }

# Modulos para chamar as classes instanciadas
module Page
  def repom
    repom ||= RepomPage.new
  end

  def prestador
    prestador ||= PrestadorPage.new
  end

  def motorista
    motorista ||= MotoristaPage.new
  end

  def veiculo
    veiculo ||= VeiculoPage.new
  end

  def prestar_contas
    prestar_contas ||= PrestadorcontasPage.new
  end

  def autorizacao
    autorizacao ||= AutorizacaoPage.new
  end

  def roteiro
    roteiro ||= RoteiroPage.new
  end

  def cancelamento_autorizacao
    cancel_autorizacao ||= CancelautorizacaoPage.new
  end

  def cancelamento_prestacao
    cancel_prestacao ||= CancelarprestacaocontasPage.new
  end

  def cancelamento_viagem
    cancel_viagem ||= CancelarviagemPage.new
  end

  def consulta_roteiro
    consultar_roteiro ||= ConsultaroteiroPage.new
  end

  def incluir_movimentos
    incluir_movimentos ||= IncluirmovimentosPage.new
  end

  def incluir_documentos
    incluir_documentos ||= IncluirdocumentosPage.new
  end

  def atendimento_transferencia
    atendimento_transferencia ||= AtendimentoPage.new
  end

  def cancelar_transferencia
    cancelar_transferencia ||= CancelamentotransferenciaPage.new
  end

  def venda_atendimento
    venda_atendimento ||= VendaatendimentoPage.new
  end

  def cancelar_venda
    cancelar_venda ||= CancelarvendaPage.new
  end

end
